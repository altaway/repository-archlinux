#!/usr/bin/env zsh

url="https://aur.archlinux.org"
repo=repository
desc=descriptions

setopt nullglob

clone() {
    cd $desc
    while read p
    do git clone $url/$p.git
    done < ../packages
    cd ..
}

build() {
    cd $desc
    for p in `ls`
    do cd $p
        makepkg -sr --noconfirm || true
        pkgfile=`ls $p*.pkg.tar.*`
        # print "pkgfile: $pkgfile"
        # tree -a ../..
        repo-add ../../$repo/altaway.db.tar.xz $pkgfile
        cp $pkgfile ../../$repo/
        cd ..
    done
   cd ..
   exit 0
}

clean() {
    rm -rf $desc $repo
    mkdir -p $desc $repo
    git clean -fd
}

# setopt
"$@" || exit 5
